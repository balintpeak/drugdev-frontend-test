# Solution

Used xstate as suggested.
The solution uses 3 machines to achieve the below task.
The `app` machine along with `AppPage` helps syncing the location with xstate, client side routing is done with react-router.
To demonstrate positive user experience for a real life app, the list of contacts are only loaded once, then cached client side.
This cache is kept up to date when a user adds / updates / removes a contact as well as synced with the server.

Given my own time constraints, I had to prioritise having a working app, so for now, only one test was added (which required some time as `create-react-app` was quite out of date).
[React Testing Library](https://testing-library.com/docs/react-testing-library/intro) was used to test the app high level.
This approach should be followed for the other pages as well.

Future improvements (if had more time):

- improve test coverage
- add browser tests with cypress
- improve machine typings
- break up `machines/contact` to smaller hierarchical machines (new / edit states) as it grew too big

## Run the app

```
npm i
npm run gql
npm start
```

---

# Contact Manager

## Features to implement

- List Contacts
- View Contact
- Add Contact
- Delete Contact
- Edit Contact
- Clean routing i.e '/contact/:id'
- Use material-ui for components

### Bonuses

- Use [xstate](https://xstate.js.org/docs) to manage app state.
- Connect to the graphql endpoint http://localhost:3001 by using create-react-app proxy feature
- Use the graphql endpoint to get/create/update/delete

### Contact

- Name eg 'John Smith'
- Email eg 'john@smith.com'
- Date Modified eg '31-01-2018 15:04'
- Date Created eg '31-01-2018 15:04'

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run gql`

Runs the graphql server.

Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
