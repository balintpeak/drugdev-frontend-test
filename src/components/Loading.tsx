import React from 'react'
import { CircularProgress, Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
}))

const Loading = () => {
  const classes = useStyles()

  return (
    <Box className={classes.root} data-testid="loading">
      <CircularProgress />
    </Box>
  )
}

export default Loading
