import React, { createContext, useContext, ReactNode } from 'react'
import { useMachine } from '@xstate/react'

import appMachineConfig from '../machines/app'

const AppState = createContext<any>(null)

const State = ({ children }: { children: ReactNode }) => {
  const appMachine = useMachine(appMachineConfig)

  return <AppState.Provider value={appMachine}>{children}</AppState.Provider>
}

const useApp = () => useContext(AppState)

export { State, useApp }
