import React, { useEffect } from 'react'
import { useService } from '@xstate/react'
import { Interpreter } from 'xstate'
import { useParams } from 'react-router-dom'
import { Container } from '@material-ui/core'

import Loading from './Loading'
import ContactCard from './ContactCard'

import { Context } from '../machines/contact'

const Contact = ({ service }: { service: Interpreter<Context> }) => {
  const [{ matches, context }, send] = useService(service)
  const { id } = useParams()

  useEffect(() => {
    if (id === 'new') {
      send('NEW')
    } else {
      send('LOAD', { id })
    }
  }, [id, send])

  return (
    <Container maxWidth="xs">
      {matches('loading') ? (
        <Loading />
      ) : (
        <ContactCard
          contact={context.contact}
          editable={matches('edit') || matches('new')}
          onEdit={() => send('EDIT')}
          onDelete={() => send('DELETE')}
          onNameChange={event =>
            send('CHANGENAME', { name: event.target.value })
          }
          onEmailChange={event =>
            send('CHANGEEMAIL', { email: event.target.value })
          }
          onSave={() => (matches('edit') ? send('UPDATE') : send('ADD'))}
        />
      )}
    </Container>
  )
}

export default Contact
