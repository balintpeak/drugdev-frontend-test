import React from 'react'
import { useHistory } from 'react-router-dom'
import {
  Card,
  CardHeader,
  Avatar,
  IconButton,
  Divider,
} from '@material-ui/core'
import { Close } from '@material-ui/icons'

import { Contact } from '../../types/Contact'
import { InputCallback, OnClick } from '../../types/React'

import EditableCard from './EditableCard'
import ViewCard from './ViewCard'

const ContactCard = ({
  contact,
  editable,
  onEdit,
  onSave,
  onDelete,
  onNameChange,
  onEmailChange,
}: {
  contact: Contact
  editable: boolean
  onEdit: OnClick
  onSave: OnClick
  onDelete: OnClick
  onNameChange: InputCallback
  onEmailChange: InputCallback
}) => {
  const { push } = useHistory()

  return (
    <Card>
      <CardHeader
        avatar={<Avatar>{contact.name[0]}</Avatar>}
        action={
          <IconButton onClick={() => push('/')}>
            <Close />
          </IconButton>
        }
        subheader={contact.id}
      />
      <Divider />
      {editable && (
        <EditableCard
          contact={contact}
          onNameChange={onNameChange}
          onEmailChange={onEmailChange}
          onSave={onSave}
        />
      )}
      {!editable && (
        <ViewCard contact={contact} onEdit={onEdit} onDelete={onDelete} />
      )}
    </Card>
  )
}

export default ContactCard
