import React from 'react'
import { CardContent, CardActions, Typography, Button } from '@material-ui/core'

import { Contact } from '../../types/Contact'
import { OnClick } from '../../types/React'

const ViewCard = ({
  contact,
  onEdit,
  onDelete,
}: {
  contact: Contact
  onEdit: OnClick
  onDelete: OnClick
}) => (
  <>
    <CardContent>
      <Typography variant="h2" variantMapping={{ h2: 'p' }}>
        {contact.name}
      </Typography>
      <Typography>{contact.email}</Typography>
    </CardContent>
    <CardActions>
      <Button size="small" variant="contained" onClick={onEdit}>
        Edit
      </Button>
      <Button size="small" color="secondary" onClick={onDelete}>
        Delete
      </Button>
    </CardActions>
  </>
)

export default ViewCard
