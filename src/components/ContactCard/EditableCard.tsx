import React from 'react'
import {
  CardContent,
  CardActions,
  TextField,
  Box,
  Button,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import { Contact } from '../../types/Contact'
import { InputCallback, OnClick } from '../../types/React'

const useStyles = makeStyles(theme => ({
  form: {
    '& > *': {
      margin: theme.spacing(2, 0),
      width: 200,
    },
  },
}))

const EditableCard = ({
  contact,
  onNameChange,
  onEmailChange,
  onSave,
}: {
  contact: Contact
  onNameChange: InputCallback
  onEmailChange: InputCallback
  onSave: OnClick
}) => {
  const classes = useStyles()

  return (
    <>
      <CardContent>
        <form className={classes.form} noValidate autoComplete="off">
          <Box>
            <TextField
              label="Name"
              value={contact.name}
              onChange={onNameChange}
            />
          </Box>
          <Box>
            <TextField
              label="Email"
              value={contact.email}
              onChange={onEmailChange}
            />
          </Box>
        </form>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          variant="contained"
          color="primary"
          onClick={onSave}
        >
          Save
        </Button>
      </CardActions>
    </>
  )
}

export default EditableCard
