import React from 'react'
import { useHistory } from 'react-router-dom'
import {
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
} from '@material-ui/core'

import { Contact } from '../types/Contact'

const ContactsList = ({ contacts }: { contacts: Contact[] }) => {
  const { push } = useHistory()

  return (
    <List>
      {contacts.map(contact => (
        <ListItem
          alignItems="flex-start"
          key={contact.id}
          button
          onClick={() => push(`/contact/${contact.id}`)}
        >
          <ListItemAvatar>
            <Avatar>{contact.name[0]}</Avatar>
          </ListItemAvatar>
          <ListItemText primary={contact.name} secondary={contact.id} />
        </ListItem>
      ))}
    </List>
  )
}

export default ContactsList
