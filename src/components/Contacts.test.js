import React from 'react'
import { wait, render } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'

import api from '../services/api'

import { State } from './State'
import AppPage from './AppPage'
import Contacts from './Contacts'

jest.mock('../services/api')

const ContactsPage = AppPage(Contacts, 'contacts')
const renderPage = () =>
  render(
    <State>
      <Router>
        <ContactsPage />
      </Router>
    </State>
  )

describe('Contacts page', () => {
  beforeEach(() => {
    api.mockClear()
  })

  test('Renders contacts', async () => {
    api.mockResolvedValue({
      data: {
        contacts: [
          {
            id: 'contact-1',
            name: 'Joe Bloggs',
            email: 'joe@tester.com',
          },
          {
            id: 'contact-2',
            name: 'Jane Bloggs',
            email: 'jane@tester.com',
          },
        ],
      },
    })

    const { getByText, getByTestId } = renderPage()

    expect(getByTestId('loading')).toBeDefined()

    await wait(() => expect(api).toHaveBeenCalledTimes(1))
    expect(api.mock.calls).toMatchSnapshot()

    expect(getByTestId('addContactButton')).toBeDefined()

    await wait(() => getByText('Joe Bloggs'))
    await wait(() => getByText('Jane Bloggs'))
  })
})
