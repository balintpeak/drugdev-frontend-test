import React, { useEffect } from 'react'
import { useApp } from './State'

/* eslint-disable react-hooks/rules-of-hooks */
const AppPage = (Component: Function, name: string) => () => {
  const [{ context }, send] = useApp()

  useEffect(() => {
    send(name.toUpperCase())
  }, [send])

  return context[name] ? <Component service={context[name]} /> : null
}

export default AppPage
