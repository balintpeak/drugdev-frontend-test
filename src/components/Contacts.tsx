import React from 'react'
import { useService } from '@xstate/react'
import { Interpreter } from 'xstate'
import { useHistory } from 'react-router-dom'
import { Container, Typography, Button } from '@material-ui/core'

import ContactsList from './ContactsList'
import Loading from './Loading'

import { Context } from '../machines/contacts'

const Contacts = ({ service }: { service: Interpreter<Context> }) => {
  const [{ matches, context }] = useService(service)
  const { push } = useHistory()

  return (
    <Container maxWidth="sm">
      <Typography variant="h1" gutterBottom>
        Contacts
      </Typography>
      {matches('loading') && <Loading />}
      {matches('loaded') && (
        <>
          <Button
            variant="contained"
            color="primary"
            onClick={() => push('/contact/new')}
            data-testid="addContactButton"
          >
            Add new
          </Button>
          <ContactsList contacts={context.contacts} />
        </>
      )}
    </Container>
  )
}

export default Contacts
