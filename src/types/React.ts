import { ChangeEvent, MouseEvent } from 'react'

export type InputCallback = (
  event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
) => void

export type OnClick = (event: MouseEvent) => void
