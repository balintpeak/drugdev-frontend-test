const api = async ({
  query = '',
  variables = {},
  operationName = '',
  headers = {},
}: {
  query: string
  variables?: object
  operationName?: string
  headers?: object
}) =>
  fetch(`/graphql`, {
    method: 'post',
    body: JSON.stringify({ operationName, variables, query }),
    headers: {
      ...headers,
      'content-type': 'application/json',
    },
  }).then(response => response.json())

export default api

export type ApiResponse<Data> = Promise<{ data: Data }>
