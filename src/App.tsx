import React from 'react'
import { Router, Switch, Route } from 'react-router-dom'

import { State } from './components/State'
import Contact from './components/Contact'
import Contacts from './components/Contacts'
import AppPage from './components/AppPage'
import history from './history'

import './App.css'

const ContactPage = AppPage(Contact, 'contact')
const ContactsPage = AppPage(Contacts, 'contacts')

const App = () => {
  return (
    <State>
      <Router history={history}>
        <Switch>
          <Route path="/contact/:id">
            <ContactPage />
          </Route>
          <Route path="/">
            <ContactsPage />
          </Route>
        </Switch>
      </Router>
    </State>
  )
}

export default App
