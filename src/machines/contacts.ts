import { Machine, assign } from 'xstate'
// @ts-ignore
import { findIndex, pathEq, lensIndex, set } from 'ramda'

import api, { ApiResponse } from '../services/api'

import { Contact } from '../types/Contact'

export interface Context {
  contacts: Contact[]
}

const contacts = Machine<Context>(
  {
    id: 'contacts',
    initial: 'loading',
    context: {
      contacts: [],
    },
    states: {
      loading: {
        invoke: {
          id: 'getContacts',
          src: 'getContacts',
          onDone: {
            target: 'loaded',
            actions: assign({
              contacts: (context, event) => event.data.data.contacts,
            }),
          },
          onError: {
            target: 'error',
          },
        },
      },
      loaded: {},
      error: {},
    },
    on: {
      SAVE: {
        actions: assign({
          contacts: (context, event) => {
            const index = findIndex(
              pathEq(['id'], event.data.id),
              context.contacts
            )

            if (index > -1) {
              return set(lensIndex(index), event.data, context.contacts)
            } else {
              return [...context.contacts, event.data]
            }
          },
        }),
      },

      DELETE: {
        actions: assign({
          contacts: (context, event) =>
            context.contacts.filter(contact => contact.id !== event.id),
        }),
      },
    },
  },
  {
    services: {
      getContacts: (): ApiResponse<{ contacts: Contact[] }> =>
        api({
          query: `
            query {
              contacts {
                id
                name
                email
              }
            }
          `,
        }),
    },
  }
)

export default contacts
