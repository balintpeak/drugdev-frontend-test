import { Machine, assign, spawn, forwardTo } from 'xstate'

import contactsMachine from './contacts'
import getContactMachine from './contact'

const app = Machine(
  {
    id: 'app',
    initial: 'idle',
    context: {
      contacts: null,
      contact: null,
    },
    states: {
      idle: {},
      contacts: {},
      contact: {
        on: {
          SAVE: {
            cond: 'haveContacts',
            actions: forwardTo(context => context.contacts),
          },
          DELETE: {
            cond: 'haveContacts',
            actions: forwardTo(context => context.contacts),
          },
        },
      },
    },
    on: {
      CONTACTS: {
        target: 'contacts',
        actions: assign({
          contacts: context => context.contacts || spawn(contactsMachine),
          contact: null,
        }),
      },

      CONTACT: {
        target: 'contact',
        actions: assign({
          contact: () => spawn(getContactMachine()),
        }),
      },
    },
  },
  {
    guards: {
      haveContacts: context => Boolean(context.contacts),
    },
  }
)

export default app
