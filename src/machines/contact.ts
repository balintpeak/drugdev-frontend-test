import { Machine, assign, sendParent } from 'xstate'

import api from '../services/api'
import history from '../history'
import { Contact } from '../types/Contact'

export interface Context {
  contact: Contact
}

const getContactMachine = () =>
  Machine<Context>(
    {
      id: 'contact',
      initial: 'initial',
      context: {
        contact: {
          id: '',
          name: '',
          email: '',
        },
      },
      states: {
        initial: {
          on: {
            LOAD: {
              target: 'loading',
              actions: assign({
                contact: (context, event) => ({
                  ...context.contact,
                  id: event.id,
                }),
              }),
            },
            NEW: 'new',
          },
        },
        new: {
          on: {
            CHANGENAME: {
              actions: assign({
                contact: (context, { name }) => ({
                  ...context.contact,
                  name,
                }),
              }),
            },
            CHANGEEMAIL: {
              actions: assign({
                contact: (context, { email }) => ({
                  ...context.contact,
                  email,
                }),
              }),
            },
            ADD: 'adding',
          },
        },

        adding: {
          invoke: {
            id: 'addContact',
            src: 'addContact',
            onDone: {
              target: 'saved',
              actions: assign({
                contact: (context, event) => event.data.data.addContact,
              }),
            },
            onError: {
              target: 'error',
            },
          },
        },

        loading: {
          invoke: {
            id: 'getContact',
            src: 'getContact',
            onDone: {
              target: 'loaded',
              actions: assign({
                contact: (context, event) => event.data.data.contact,
              }),
            },
            onError: {
              target: 'error',
            },
          },
        },

        loaded: {
          on: {
            EDIT: 'edit',
            DELETE: 'deleting',
          },
        },

        edit: {
          on: {
            CHANGENAME: {
              actions: assign({
                contact: (context, { name }) => ({
                  ...context.contact,
                  name,
                }),
              }),
            },
            CHANGEEMAIL: {
              actions: assign({
                contact: (context, { email }) => ({
                  ...context.contact,
                  email,
                }),
              }),
            },
            UPDATE: 'updating',
          },
        },

        updating: {
          invoke: {
            id: 'updateContact',
            src: 'updateContact',
            onDone: {
              target: 'saved',
            },
            onError: {
              target: 'error',
            },
          },
        },

        deleting: {
          invoke: {
            id: 'deleteContact',
            src: 'deleteContact',
            onDone: {
              target: 'deleted',
            },
            onError: {
              target: 'error',
            },
          },
        },

        saved: {
          type: 'final',
          entry: [
            sendParent((context: Context) => ({
              type: 'SAVE',
              data: context.contact,
            })),
            'goToContacts',
          ],
        },

        deleted: {
          type: 'final',
          entry: [
            sendParent((context: Context) => ({
              type: 'DELETE',
              id: context.contact.id,
            })),
            'goToContacts',
          ],
        },

        error: {},
      },
    },
    {
      actions: {
        goToContacts: () => {
          history.push('/')
        },
      },
      services: {
        getContact: ({ contact: { id } }) =>
          api({
            query: `
              query ($id: ID){
                contact(id: $id) {
                  id
                  name
                  email
                }
              }
            `,
            variables: { id },
          }),

        addContact: ({ contact }) =>
          api({
            query: `
              mutation ($contact: InputContact){
                addContact(contact: $contact) {
                  id
                  name
                  email
                }
              }
            `,
            variables: { contact },
          }),

        updateContact: ({ contact }) =>
          api({
            query: `
              mutation ($contact: InputContact){
                updateContact(contact: $contact) {
                  id
                  name
                  email
                }
              }
            `,
            variables: { contact },
          }),

        deleteContact: ({ contact: { id } }) =>
          api({
            query: `
              mutation ($id: ID){
                deleteContact(id: $id)
              }
            `,
            variables: { id },
          }),
      },
    }
  )

export default getContactMachine
